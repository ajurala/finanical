package com.aj.finanical;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc41 extends Calc {   

	EditText futureValue = null;
    EditText interest = null;
    EditText duration = null;
    
    ResultTextView result = null;
    
    TextView futureValue_name = null;
    TextView interest_name = null;
    TextView duration_name = null;
    
    TextView result_name = null;
    
    RelativeLayout calc_result_field = null;
    RelativeLayout calc_result_value = null;    
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc41_future_value);    	
    	futureValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = calc_options.equals("0") ? getString(R.string.calc41_0_future_value_name): getString(R.string.calc41_1_future_value_name);    	
    	futureValue_name.setText(value);
    	
    	futureValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Interest
    	v = findViewById(R.id.calc41_percent_value);    	
    	interest_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = calc_options.equals("0") ? getString(R.string.calc41_0_inflation_name): getString(R.string.calc41_1_interest_name);    	
    	interest_name.setText(value);

    	interest = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration
    	v = findViewById(R.id.calc41_duration_value);    	
    	duration_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = calc_options.equals("0") ? getString(R.string.calc41_0_duration_name): getString(R.string.calc41_1_duration_name);    	
    	duration_name.setText(value);
    	
    	duration = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Result
    	v = findViewById(R.id.calc41_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = calc_options.equals("0") ? getString(R.string.calc41_0_result_name): getString(R.string.calc41_1_result_name);    	
    	result_name.setText(value);

    	result = (ResultTextView) v.findViewById(R.id.calc_field_value);
    	
    	calc_result_field = (RelativeLayout) v.findViewById(R.id.calc_field);
    	calc_result_value = (RelativeLayout) v.findViewById(R.id.calc_value);
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				
				calculationNeeded = true;
			}
		};
		
    	futureValue.addTextChangedListener(editTextWatcher);
    	interest.addTextChangedListener(editTextWatcher);
    	duration.addTextChangedListener(editTextWatcher);
    	
    	setResultView(calc_result_field, calc_result_value, result);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double fv, intrst, cv;
		int years;
		
		try{
			fv = Double.parseDouble(futureValue.getText().toString());
			intrst = Double.parseDouble(interest.getText().toString());
			years = Integer.parseInt(duration.getText().toString());
			
			cv = fv/(Math.pow(1.0+ (intrst/100.0), years));
		}catch(NumberFormatException e) {
			cv = 0;
		}
		
		setResultText(cv);
	}
}
