package com.aj.finanical;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc01 extends Calc {   

	EditText investmentValue = null;
	EditText yearly_increase = null;
    EditText returns_expected = null;
    EditText duration_years = null;
    EditText duration_months = null;
    EditText returns_expected2 = null;
    EditText duration_years2 = null;
    EditText duration_months2 = null;
    Spinner compound = null;
    
    TextView investmentValue_name = null;
    TextView yearly_increase_name = null;
    TextView returns_expected_name = null;
    TextView duration_years_name = null;
    TextView duration_months_name = null;
    TextView returns_expected_name2 = null;
    TextView duration_years_name2 = null;
    TextView duration_months_name2 = null;
    TextView compound_name = null;
    
    TextView result_name = null;
    
    View yearly_increase_view = null;
    View duration_years_view2 = null;
    View duration_months_view2 = null;
    View returns_expected_view2 = null;
    View results_view2 = null;
    View results_view3 = null;
    
    ArrayList<RelativeLayout> calc_result_fields = new ArrayList<RelativeLayout>();
    ArrayList<RelativeLayout> calc_result_values = new ArrayList<RelativeLayout>();
    ArrayList<ResultTextView> results = new ArrayList<ResultTextView>();

    String options;
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc01_per_investment_value);    	
    	investmentValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc01_0_per_investment_value_name);    	
    	investmentValue_name.setText(value);
    	
    	investmentValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Yearly Increase
    	v = findViewById(R.id.calc01_yearly_increase_value);    	
    	yearly_increase_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc01_0_yearly_increase_name);    	
    	yearly_increase_name.setText(value);
    	
    	yearly_increase = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	yearly_increase_view = v;
    	
    	//Returns expected
    	v = findViewById(R.id.calc01_returns_expected_value);    	
    	returns_expected_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_returns_expected_name);    	
    	returns_expected_name.setText(value);

    	returns_expected = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in Years
    	v = findViewById(R.id.calc01_duration_years_value);    	
    	duration_years_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_duration_years_name);    	
    	duration_years_name.setText(value);
    	
    	duration_years = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in months
    	v = findViewById(R.id.calc01_duration_months_value);    	
    	duration_months_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_duration_months_name);    	
    	duration_months_name.setText(value);
    	
    	duration_months = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Returns expected 2
    	v = findViewById(R.id.calc01_returns_expected_value2);    	
    	returns_expected_name2 = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_returns_expected_name2);    	
    	returns_expected_name2.setText(value);

    	returns_expected2 = (EditText) v.findViewById(R.id.calc_field_value);    	

    	returns_expected_view2 = v;
    	
    	//Duration in Years2
    	v = findViewById(R.id.calc01_duration_years_value2);    	
    	duration_years_name2 = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_duration_years_name2);    	
    	duration_years_name2.setText(value);
    	
    	duration_years2 = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	duration_years_view2 = v;
    	
    	//Duration in months2
    	v = findViewById(R.id.calc01_duration_months_value2);    	
    	duration_months_name2 = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_duration_months_name2);    	
    	duration_months_name2.setText(value);
    	
    	duration_months2 = (EditText) v.findViewById(R.id.calc_field_value);    	
    	
    	duration_months_view2 = v;
    	
    	//Compounded
    	v = findViewById(R.id.calc01_compound_iteration_value);   
    	compound_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_compound_name);    	
    	compound_name.setText(value);
    	
    	compound = (Spinner) v.findViewById(R.id.calc_field_value);
    	
    	//Result List
    	//Result 1
    	v = findViewById(R.id.calc01_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_result_name);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 2
    	v = findViewById(R.id.calc01_result_value2);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_result_name2);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	results_view2 = v;
    	
    	//Result 3
    	v = findViewById(R.id.calc01_result_value3);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_result_name3);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	results_view3 = v;
    	
    	/*//Result 4
    	v = findViewById(R.id.calc01_result_value4);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc01_0_result_name4);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	*/
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
		OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?>  parent, View view, int position, long id) {
				
				String value = compound.getItemAtPosition(position).toString() +" "+getString(R.string.calc01_0_per_investment_value_name);    	
		    	investmentValue_name.setText(value);
		    	
		    	//calculateResult();
		    	calculationNeeded = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		};
		
    	investmentValue.addTextChangedListener(editTextWatcher);
    	yearly_increase.addTextChangedListener(editTextWatcher);
    	
    	returns_expected.addTextChangedListener(editTextWatcher);
    	duration_years.addTextChangedListener(editTextWatcher);
    	duration_months.addTextChangedListener(editTextWatcher);
    	
    	returns_expected2.addTextChangedListener(editTextWatcher);
    	duration_years2.addTextChangedListener(editTextWatcher);
    	duration_months2.addTextChangedListener(editTextWatcher);
    	
    	compound.setOnItemSelectedListener(itemSelectedListener);    	
    	compound.setSelection(Integer.parseInt(calc_options.substring(calc_options.indexOf('.') + 1)) - 1);
    	
    	options = calc_options.substring(0, calc_options.indexOf('.'));
    	
    	Log.v("AUKOPT", options);
    	
    	if(options.equals("0")) {    		
    		yearly_increase_view.setVisibility(View.GONE);   	
    		
    		duration_months_view2.setVisibility(View.GONE);
    		duration_years_view2.setVisibility(View.GONE);
    		returns_expected_view2.setVisibility(View.GONE);
    		
    		results_view2.setVisibility(View.GONE);
    		//results_view3.setVisibility(View.GONE);
    	}else if(options.equals("1")) {    		
    		duration_months_view2.setVisibility(View.GONE);
    		duration_years_view2.setVisibility(View.GONE);
    		returns_expected_view2.setVisibility(View.GONE);
    		
    		results_view2.setVisibility(View.GONE);
    		//results_view3.setVisibility(View.GONE);
    	}else if(options.equals("2")) {
    		yearly_increase_view.setVisibility(View.GONE);
    	}
    	
    	setResultViews(calc_result_fields, calc_result_values, results);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		duration_years.addTextChangedListener(new YearsMonthsTextWatcher(duration_months, false));
    	duration_months.addTextChangedListener(new YearsMonthsTextWatcher(duration_years, true));
    	
    	duration_years2.addTextChangedListener(new YearsMonthsTextWatcher(duration_months2, false));
    	duration_months2.addTextChangedListener(new YearsMonthsTextWatcher(duration_years2, true));
	}	
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}
	
	protected void calculateResult() {
		double iv, returns_expctd, returns_expctd2, tenure, res, sip =0, amount = 0, fv = 0, yrly_increase = 0;
		int months, months2;
	
		
		try{
			iv = Double.parseDouble(investmentValue.getText().toString());
			returns_expctd = Double.parseDouble(returns_expected.getText().toString()) / 100;
			months = Integer.parseInt(duration_months.getText().toString());			
			
			if(iv == 0
				|| returns_expctd == 0
				|| months == 0) {
				res = 0;
			}
			else {
				int selectedPosition = compound.getSelectedItemPosition();
				int[] multiplier = getResources().getIntArray(R.array.compound_iteration_multiplier);
				double yrly_percent = 0;
				
				tenure =  months * multiplier[selectedPosition] / 12.0;
				returns_expctd = returns_expctd / multiplier[selectedPosition];
				
				if(options.equals("1")
						|| options.equals("3")) {
						yrly_percent = yrly_increase = Double.parseDouble(yearly_increase.getText().toString()) / 100;
						yrly_increase = yrly_increase / multiplier[selectedPosition];
				}
				
				double rate = 1 + yrly_percent;
				if(rate == 1)
					amount = tenure * iv;
				else
					amount = multiplier[selectedPosition] * iv * ((1 - Math.pow(rate, months/12.0))/ (1 - rate));
				
				if (returns_expctd==yrly_increase)
				{
					returns_expctd = yrly_increase+.00001;
				}
				
				res = iv * (1 + returns_expctd) * (Math.pow((1 + returns_expctd), tenure) - Math.pow((1 + yrly_increase), tenure))/(returns_expctd - yrly_increase);
				
				if(options.equals("2")
						|| options.equals("3")) {
					returns_expctd2 = Double.parseDouble(returns_expected2.getText().toString()) / 100;
					months2 = Integer.parseInt(duration_months2.getText().toString());	
					
					if(returns_expctd2 == 0
						|| months2 == 0
						|| months2 <= months) {
						res = 0;
					}
					else {
						months2 = months2 - months;
						tenure =  months2 * multiplier[selectedPosition] / 12.0;
						returns_expctd2 = returns_expctd2 / multiplier[selectedPosition];
						
						res = res * Math.pow(1 + returns_expctd2, tenure);
					}
				}
			}
			
			
		}catch(NumberFormatException e) {
			res = 0;
		}
		
		NumberFormat f = NumberFormat.getInstance(Locale.US);		
		
		results.get(0).setText(f.format(res));
		
		if(results_view2.getVisibility() == View.VISIBLE)
			results.get(1).setText(f.format(sip));
		
		results.get(2).setText(f.format(amount));
		
		//results.get(3).setText(f.format(principal_paid) + " (" + f.format(principal_percent) + "%)");
		
		
	}
	
	private class YearsMonthsTextWatcher implements TextWatcher {

	    private EditText et; 
	    private boolean month;

	    private YearsMonthsTextWatcher(EditText et, boolean month) {
	        this.et = et; 
	        this.month = month;
	    }

	    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)         {}
	    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) 	    {}

	    public void afterTextChanged(Editable editable) {

	        String text = editable.toString();
	        int val;
	        
	        try{
				
				val = Integer.parseInt(text);
				if(month)
					val = val / 12;
				else
					val = val * 12;
			}catch(NumberFormatException e) {
				val = 0;
			}
	        
	        text = String.valueOf(val);
	        
	        /*if(et.isFocused() && month)
	        	Log.v("AUK","year is focused");
	        if(et.isFocused() && !month)
	        	Log.v("AUK","month is focused");
	        
	        if(duration_years.isFocused())
	        	Log.v("AUK", "YEARS is focused");	        
	        if(duration_months.isFocused())
	        	Log.v("AUK", "MONTHS is focused");*/
	        
            if(!text.equals("") && !et.isFocused())
                et.setText(text);
	    }
	}
}
