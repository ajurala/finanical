package com.aj.finanical;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc21 extends Calc {   

	EditText loanValue = null;
    EditText interest = null;
    EditText duration_years = null;
    EditText duration_months = null;
    Spinner compound = null;
    
    TextView loanValue_name = null;
    TextView interest_name = null;
    TextView duration_years_name = null;
    TextView duration_months_name = null;
    TextView compound_name = null;
    
    TextView result_name = null;
    
    ArrayList<RelativeLayout> calc_result_fields = new ArrayList<RelativeLayout>();
    ArrayList<RelativeLayout> calc_result_values = new ArrayList<RelativeLayout>();
    ArrayList<ResultTextView> results = new ArrayList<ResultTextView>();
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc21_loan_value);    	
    	loanValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc21_0_loan_value_name);    	
    	loanValue_name.setText(value);
    	
    	loanValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Interest
    	v = findViewById(R.id.calc21_percent_value);    	
    	interest_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_interest_name);    	
    	interest_name.setText(value);

    	interest = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in Years
    	v = findViewById(R.id.calc21_duration_years_value);    	
    	duration_years_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_duration_years_name);    	
    	duration_years_name.setText(value);
    	
    	duration_years = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in months
    	v = findViewById(R.id.calc21_duration_months_value);    	
    	duration_months_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_duration_months_name);    	
    	duration_months_name.setText(value);
    	
    	duration_months = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Compounded
    	v = findViewById(R.id.calc21_compound_iteration_value);   
    	compound_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_compound_name);    	
    	compound_name.setText(value);
    	
    	compound = (Spinner) v.findViewById(R.id.calc_field_value);
    	
    	//Result List
    	//Result 1
    	v = findViewById(R.id.calc21_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_result_name);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 2
    	v = findViewById(R.id.calc21_result_value2);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_result_name2);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 3
    	v = findViewById(R.id.calc21_result_value3);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_result_name3);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 4
    	v = findViewById(R.id.calc21_result_value4);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc21_0_result_name4);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
		OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?>  parent, View view, int position, long id) {				
		    	//calculateResult();
				calculationNeeded = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		};
		
    	loanValue.addTextChangedListener(editTextWatcher);
    	interest.addTextChangedListener(editTextWatcher);
    	duration_years.addTextChangedListener(editTextWatcher);
    	duration_months.addTextChangedListener(editTextWatcher);
    	
    	compound.setOnItemSelectedListener(itemSelectedListener);
    	
    	compound.setSelection(Integer.parseInt(calc_options.substring(calc_options.indexOf('.') + 1)) - 1);
    	
    	setResultViews(calc_result_fields, calc_result_values, results);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		duration_years.addTextChangedListener(new YearsMonthsTextWatcher(duration_months, false));
    	duration_months.addTextChangedListener(new YearsMonthsTextWatcher(duration_years, true));
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double lv, intrst, emi, amount, interest_paid, principal_paid, interest_percent, principal_percent, tenure;
		int months;
	
		
		try{
			lv = Double.parseDouble(loanValue.getText().toString());
			intrst = Double.parseDouble(interest.getText().toString()) / 100;
			months = Integer.parseInt(duration_months.getText().toString());
			if(lv == 0
				|| intrst == 0 
				||	months == 0)
			{
				emi = amount = interest_paid = principal_paid = interest_percent = principal_percent = 0;
			}
			else
			{
				int selectedPosition = compound.getSelectedItemPosition();
				int[] multiplier = getResources().getIntArray(R.array.compound_iteration_multiplier);
				
				tenure =  months * multiplier[selectedPosition] / 12.0;
				intrst = intrst / multiplier[selectedPosition];
				
				emi = lv * intrst * Math.pow((1+intrst), tenure) / (Math.pow((1+intrst),tenure) -1);
						
				amount = emi * tenure;
				principal_paid = lv;
				interest_paid =  amount - lv;
				
				interest_percent = (interest_paid / amount) * 100;
				principal_percent = (principal_paid / amount) * 100;
				
				emi = emi * multiplier[selectedPosition] / 12;
			}
			
		}catch(NumberFormatException e) {
			emi = amount = interest_paid = principal_paid = interest_percent = principal_percent = 0;
		}
		
		NumberFormat f = NumberFormat.getInstance(Locale.US);		
		
		results.get(0).setText(f.format(emi));
		results.get(1).setText(f.format(amount));
		results.get(2).setText(f.format(interest_paid) + " (" + f.format(interest_percent) + "%)");
		results.get(3).setText(f.format(principal_paid) + " (" + f.format(principal_percent) + "%)");
		
		
	}
	
	private class YearsMonthsTextWatcher implements TextWatcher {

	    private EditText et; 
	    private boolean month;

	    private YearsMonthsTextWatcher(EditText et, boolean month) {
	        this.et = et; 
	        this.month = month;
	    }

	    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)         {}
	    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) 	    {}

	    public void afterTextChanged(Editable editable) {

	        String text = editable.toString();
	        int val;
	        
	        try{
				
				val = Integer.parseInt(text);
				if(month)
					val = val / 12;
				else
					val = val * 12;
			}catch(NumberFormatException e) {
				val = 0;
			}
	        
	        text = String.valueOf(val);
	        
	        /*if(et.isFocused() && month)
	        	Log.v("AUK","year is focused");
	        if(et.isFocused() && !month)
	        	Log.v("AUK","month is focused");
	        
	        if(duration_years.isFocused())
	        	Log.v("AUK", "YEARS is focused");	        
	        if(duration_months.isFocused())
	        	Log.v("AUK", "MONTHS is focused");*/
	        
            if(!text.equals("") && !et.isFocused())
                et.setText(text);
	    }
	}
}
