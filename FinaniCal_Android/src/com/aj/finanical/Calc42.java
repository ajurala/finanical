package com.aj.finanical;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc42 extends Calc {   

	EditText currentValue = null;
    EditText interest = null;
    EditText duration = null;
    Spinner compound = null;
    
    ResultTextView result = null;
    
    TextView currentValue_name = null;
    TextView interest_name = null;
    TextView duration_name = null;
    TextView compound_name = null;
    
    TextView result_name = null;
    
    RelativeLayout calc_result_field = null;
    RelativeLayout calc_result_value = null;    
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc42_current_value);    	
    	currentValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc42_0_future_value_name);   	
    	currentValue_name.setText(value);
    	
    	currentValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Interest
    	v = findViewById(R.id.calc42_percent_value);    	
    	interest_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc42_0_inflation_name);    	
    	interest_name.setText(value);

    	interest = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration
    	v = findViewById(R.id.calc42_duration_value);    	
    	duration_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc42_0_duration_name);    	
    	duration_name.setText(value);
    	
    	duration = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Compounded
    	v = findViewById(R.id.calc42_compound_iteration_value);   
    	compound_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc42_0_compound_name);    	
    	compound_name.setText(value);
    	
    	compound = (Spinner) v.findViewById(R.id.calc_field_value);
    	
    	//Result
    	v = findViewById(R.id.calc42_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc42_0_result_name);    	
    	result_name.setText(value);

    	result = (ResultTextView) v.findViewById(R.id.calc_field_value);
    	
    	calc_result_field = (RelativeLayout) v.findViewById(R.id.calc_field);
    	calc_result_value = (RelativeLayout) v.findViewById(R.id.calc_value);
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
    	currentValue.addTextChangedListener(editTextWatcher);
    	interest.addTextChangedListener(editTextWatcher);
    	duration.addTextChangedListener(editTextWatcher);
    	
    	compound.setSelection(Integer.parseInt(calc_options.substring(calc_options.indexOf('.') + 1)) - 1);
    	
    	setResultView(calc_result_field, calc_result_value, result);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double cv, intrst, fv;
		int years;
		
		try{
			cv = Double.parseDouble(currentValue.getText().toString());
			intrst = Double.parseDouble(interest.getText().toString());
			years = Integer.parseInt(duration.getText().toString());			
			
			int selectedPosition = compound.getSelectedItemPosition();
			int[] multiplier = getResources().getIntArray(R.array.compound_iteration_multiplier);
			
			int iterations =  years * multiplier[selectedPosition];
			intrst = intrst / multiplier[selectedPosition];
			
			fv = cv * (Math.pow(1.0+ (intrst/100.0), iterations));
		}catch(NumberFormatException e) {
			fv = 0;
		}
		
		setResultText(fv);
	}
}
