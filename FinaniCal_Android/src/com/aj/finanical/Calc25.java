package com.aj.finanical;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.aj.finanical.adapters.AmortizationListAdapter;
import com.aj.finanical.adapters.Amortize;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DateTimeKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc25 extends Calc {   

	EditText loanValue = null;
    EditText interest = null;
    EditText duration_years = null;
    EditText duration_months = null;
    Spinner compound = null;
    
    TextView loanValue_name = null;
    TextView interest_name = null;
    TextView duration_years_name = null;
    TextView duration_months_name = null;
    TextView compound_name = null;
    
    TextView result_name = null;
    
    ArrayList<RelativeLayout> calc_result_fields = new ArrayList<RelativeLayout>();
    ArrayList<RelativeLayout> calc_result_values = new ArrayList<RelativeLayout>();
    ArrayList<ResultTextView> results = new ArrayList<ResultTextView>();
    
    LinearLayout amortizationLayout = null;
    
    //ExpandableListView amortizationListView = null;
    
    //TODO - A Semaphore might be needed
        
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc25_loan_value);    	
    	loanValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc25_0_loan_value_name);    	
    	loanValue_name.setText(value);
    	
    	loanValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Interest
    	v = findViewById(R.id.calc25_percent_value);    	
    	interest_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_interest_name);    	
    	interest_name.setText(value);

    	interest = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in Years
    	v = findViewById(R.id.calc25_duration_years_value);    	
    	duration_years_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_duration_years_name);    	
    	duration_years_name.setText(value);
    	
    	duration_years = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration in months
    	v = findViewById(R.id.calc25_duration_months_value);    	
    	duration_months_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_duration_months_name);    	
    	duration_months_name.setText(value);
    	
    	duration_months = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Compounded
    	v = findViewById(R.id.calc25_compound_iteration_value);   
    	compound_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_compound_name);    	
    	compound_name.setText(value);
    	
    	compound = (Spinner) v.findViewById(R.id.calc_field_value);
    	
    	//Result List
    	//Result 1
    	v = findViewById(R.id.calc25_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_result_name);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 2
    	v = findViewById(R.id.calc25_result_value2);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc25_0_result_name2);    	
    	result_name.setText(value);
    	
    	//amortizationListView = (ExpandableListView)v.findViewById(R.id.amortization_list);    
    	
    	//Result 3
    	amortizationLayout = (LinearLayout)findViewById(R.id.calc25_result_value3);
    	
    	//The header texts
    	v = findViewById(R.id.calc25_result_header_value);
    	
    	result_name = (TextView)v.findViewById(R.id.month);
    	value = getString(R.string.calc25_0_result_month);   
    	result_name.setText(value);
    	
    	result_name = (TextView)v.findViewById(R.id.interest);
    	value = getString(R.string.calc25_0_result_interest);   
    	result_name.setText(value);
    	
    	result_name = (TextView)v.findViewById(R.id.principal);
    	value = getString(R.string.calc25_0_result_principal);   
    	result_name.setText(value);
    	
    	result_name = (TextView)v.findViewById(R.id.balance);
    	value = getString(R.string.calc25_0_result_balance);   
    	result_name.setText(value);
    	
    		
    	    	
    	
    	
    	setResultViews(calc_result_fields, calc_result_values, results);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
		OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?>  parent, View view, int position, long id) {				
		    	//calculateResult();
				calculationNeeded = true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		};
		
    	loanValue.addTextChangedListener(editTextWatcher);
    	interest.addTextChangedListener(editTextWatcher);
    	duration_years.addTextChangedListener(editTextWatcher);
    	duration_months.addTextChangedListener(editTextWatcher);
    	
    	compound.setOnItemSelectedListener(itemSelectedListener);
    	
    	compound.setSelection(Integer.parseInt(calc_options.substring(calc_options.indexOf('.') + 1)) - 1);
		
		duration_years.addTextChangedListener(new YearsMonthsTextWatcher(duration_months, false));
    	duration_months.addTextChangedListener(new YearsMonthsTextWatcher(duration_years, true));
    	
    	//calculateResult();
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/	
	
	
	protected void calculateResult() {		
		startCalculation();
	}
	
	void startCalculation() {
		double lv, intrst, emi, tenure;
		int months;
		
		Map<String, List<Amortize>> amortizationCollections = new LinkedHashMap<String, List<Amortize>>();
	    List<Amortize> amortization = new ArrayList<Amortize>();
	
	    NumberFormat f = NumberFormat.getInstance(Locale.US);
		Log.v("AUK Start Calc", new Date().toString());
		try{
			lv = Double.parseDouble(loanValue.getText().toString());
			intrst = Double.parseDouble(interest.getText().toString()) / 100;
			months = Integer.parseInt(duration_months.getText().toString());
			int selectedPosition = compound.getSelectedItemPosition();
			if(lv == 0
				|| intrst == 0 
				||	months == 0
				|| selectedPosition < 0)
			{
				emi = 0;
			}
			else
			{				
				int[] multiplier = getResources().getIntArray(R.array.compound_iteration_multiplier);
				
				tenure =  months * multiplier[selectedPosition] / 12.0;
				intrst = intrst / multiplier[selectedPosition];
				
				emi = lv * intrst * Math.pow((1+intrst), tenure) / (Math.pow((1+intrst),tenure) -1);
				
				emi = emi * multiplier[selectedPosition] / 12;
				
				/* Populate the amortization now */
				double principal_sum = 0;
				double interest_sum = 0;
				double balance = lv;
				double r = 1+intrst;
				
				int financial_count = 1;
				int year_count = 1;
				
				List<Amortize> amCollection = new ArrayList<Amortize>();
				
				for(int i = 0; i < tenure; ++i) {
					
					double principal = Math.pow(r, i) * (emi - (lv*(r-1)));
					double interest = emi - principal;
					
					principal_sum += principal;
					interest_sum += interest;
					balance -= principal;
					
					Amortize am = new Amortize("", f.format(interest), f.format(principal), f.format(balance));
					amCollection.add(am);
					
					if(financial_count * 12.0 / multiplier[selectedPosition] == 12.0) {
						am = new Amortize(String.valueOf(year_count), f.format(interest_sum), f.format(principal_sum), "");
						amortization.add(am);
						amortizationCollections.put(String.valueOf(year_count), amCollection);
						
						amCollection = new ArrayList<Amortize>();
						
						financial_count = 1;
						principal_sum = 0;
						interest_sum = 0;
						
						year_count++;
					}
					else financial_count++;
					
					//Log.v("AUKAM Finc", String.valueOf(financial_count));
					//Log.v("AUKAM Year", String.valueOf(year_count));
				}
				
				
			}
			
		}catch(NumberFormatException e) {
			emi = 0;
		}
		
		Log.v("AUK Finish Calc", new Date().toString());
		results.get(0).setText(f.format(emi));
	    
		amortizationLayout.removeAllViews();
		Log.v("AUK Removed all views", new Date().toString());
		/* Now add the new views and listeners to them */
		LayoutInflater infalInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
		for(int i = 0; i < amortization.size(); ++i) {
			Amortize am = amortization.get(i);
			String value;
			
			LinearLayout amortization_sub_layout = (LinearLayout)infalInflater.inflate(R.layout.amortization_list,
					amortizationLayout, false);			
			
			//Filling up summary
			result_name = (TextView)amortization_sub_layout.findViewById(R.id.month);
	    	value = am.getMonth();   
	    	result_name.setText(value);
	    	
	    	result_name = (TextView)amortization_sub_layout.findViewById(R.id.interest);
	    	value = am.getInterest();   
	    	result_name.setText(value);
	    	
	    	result_name = (TextView)amortization_sub_layout.findViewById(R.id.principal);
	    	value = am.getPrincipal();   
	    	result_name.setText(value);
	    	
	    	result_name = (TextView)amortization_sub_layout.findViewById(R.id.balance);
	    	value = am.getBalance();   
	    	result_name.setText(value);
	    	
	    	//Get the actual list now
	    	
	    	LinearLayout amortization_list_layout = (LinearLayout)amortization_sub_layout.findViewById(R.id.amortization_list);
	    	
	    	List<Amortize> amList= amortizationCollections.get(am.getMonth());
	    	for(int j = 0; j < amList.size(); ++j) {
	    		am = amList.get(j);
	    		
	    		LinearLayout amortization_individual_layout = (LinearLayout)infalInflater.inflate(R.layout.amortization,
						amortizationLayout, false);			
				
				result_name = (TextView)amortization_individual_layout.findViewById(R.id.month);
		    	value = am.getMonth();   
		    	result_name.setText(value);
		    	
		    	result_name = (TextView)amortization_individual_layout.findViewById(R.id.interest);
		    	value = am.getInterest();   
		    	result_name.setText(value);
		    	
		    	result_name = (TextView)amortization_individual_layout.findViewById(R.id.principal);
		    	value = am.getPrincipal();   
		    	result_name.setText(value);
		    	
		    	result_name = (TextView)amortization_individual_layout.findViewById(R.id.balance);
		    	value = am.getBalance();   
		    	result_name.setText(value);
		    	
		    	amortization_list_layout.addView(amortization_individual_layout);
	    	}
	    	
	    	// Attach a listener which will handle the clicks to the summary layout and show arrows and show/hides details
	    	amortizationLayout.addView(amortization_sub_layout);
		}
		
		calculationNeeded = false;
		
		Log.v("AUK Added all views", new Date().toString());
		
	}
	
	private class YearsMonthsTextWatcher implements TextWatcher {

	    private EditText et; 
	    private boolean month;

	    private YearsMonthsTextWatcher(EditText et, boolean month) {
	        this.et = et; 
	        this.month = month;
	    }

	    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)         {}
	    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) 	    {}

	    public void afterTextChanged(Editable editable) {

	        String text = editable.toString();
	        int val;
	        
	        try{
				
				val = Integer.parseInt(text);
				if(month)
					val = val / 12;
				else
					val = val * 12;
			}catch(NumberFormatException e) {
				val = 0;
			}
	        
	        text = String.valueOf(val);
	        
	        /*if(et.isFocused() && month)
	        	Log.v("AUK","year is focused");
	        if(et.isFocused() && !month)
	        	Log.v("AUK","month is focused");
	        
	        if(duration_years.isFocused())
	        	Log.v("AUK", "YEARS is focused");	        
	        if(duration_months.isFocused())
	        	Log.v("AUK", "MONTHS is focused");*/
	        
            if(!text.equals("") && !et.isFocused())
                et.setText(text);
	    }
	}
}
