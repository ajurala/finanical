package com.aj.finanical;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class ResultTextView extends TextView {

	OnTextViewSizeChangedListener mListener;
	
	public ResultTextView(Context context) {
		super(context);		
	}
	
	public ResultTextView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	}

	public void setCustomEventListener(OnTextViewSizeChangedListener eventListener) {
		mListener=eventListener;
	}
	
	protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld)
    {
        super.onSizeChanged(xNew, yNew, xOld, yOld);

        if(mListener != null)
        {
        	mListener.onTextViewSizeChangedEvent();
        }
    }
	
	public interface OnTextViewSizeChangedListener {
		public void onTextViewSizeChangedEvent();
	}

}

