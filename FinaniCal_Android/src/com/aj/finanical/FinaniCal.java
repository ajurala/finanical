package com.aj.finanical;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import com.aj.finanical.adapters.AllListAdapter;
import com.aj.finanical.adapters.CategoryListAdapter;
import com.aj.finanical.adapters.FavoriteListAdapter;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class FinaniCal extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	
	public void updateFavoriteList() {
		mSectionsPagerAdapter.updateFavoriteList();
	}
	
	public void updateCategoryList() {
		mSectionsPagerAdapter.updateCategoryList();
	}
	
	public void updateAllCalcsList() {
		mSectionsPagerAdapter.updateAllCalcsList();
	}	
	
	@Override
	public void onPause() {
		super.onPause();					
		
		SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE).edit();		
		
		editor.putInt(getString(R.string.finaniCal_view_page), mViewPager.getCurrentItem());
        	
		editor.commit();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE);
		mViewPager.setCurrentItem(sharedPrefs.getInt(getString(R.string.finaniCal_view_page), 0), true);
		
		//TODO - Remove this line
		//showCalc("41.0", "Current Value of Future Money");
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_finanical);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.finanical, menu);
		return true;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	}
	
	public void showCalc(String calcOptions, String calcName, String category) {
		String[] optionsList = calcOptions.split("\\.", 2);		
		
		/* Generate the class/activity to be used from the first */		
		String className = getPackageName()+".Calc"+optionsList[0];		
		
		Class<?> c = null;
		
	    try {
	        c = Class.forName(className);
	    } catch (ClassNotFoundException e) {
	        //Log.v("AUK", "class not found");
	        e.printStackTrace();
	    }
		
	    if(c != null) {
			Intent intent = new Intent(this, c);
			
			intent.putExtra(getString(R.string.options_to_calc), optionsList[1]);
			intent.putExtra(getString(R.string.name_of_calc), calcName);
			intent.putExtra(getString(R.string.category_of_calc), category);
			startActivity(intent);
	    }
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		
		FavoriteSectionFragment favSectionFragment;
		CategorySectionFragment catSectionFragment;
		AllSectionFragment allSectionFragment;

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}
		
		public void updateFavoriteList() {
			if(favSectionFragment != null) {
				favSectionFragment.updateFavoriteList();
			}
		}
		
		public void updateCategoryList() {
			if(catSectionFragment != null) {
				catSectionFragment.updateCategoryListView();
			}
		}
		
		public void updateAllCalcsList() {
			if(allSectionFragment != null) {
				allSectionFragment.updateAllCalcsListView();
			}
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			if(position == 0) {
				favSectionFragment = new FavoriteSectionFragment();
				Bundle args = new Bundle();
				args.putInt(FavoriteSectionFragment.ARG_SECTION_NUMBER, position + 1);
				favSectionFragment.setArguments(args);				
				
				return favSectionFragment;
			}
			else if(position == 1) {
				catSectionFragment = new CategorySectionFragment();
				Bundle args = new Bundle();
				args.putInt(CategorySectionFragment.ARG_SECTION_NUMBER, position + 1);
				catSectionFragment.setArguments(args);
				
				return catSectionFragment;
			}
			else if(position == 2) {
				allSectionFragment = new AllSectionFragment();
				Bundle args = new Bundle();
				args.putInt(AllSectionFragment.ARG_SECTION_NUMBER, position + 1);
				allSectionFragment.setArguments(args);
				
				return allSectionFragment;
			}
			else {
				// Return a DummySectionFragment (defined as a static inner class
				// below) with the page number as its lone argument.

				Fragment fragment = new DummySectionFragment();
				Bundle args = new Bundle();
				args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
				fragment.setArguments(args);
				return fragment;
			}
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
		
		@Override
		public void setPrimaryItem (ViewGroup container, int position, Object object) {
			int resId = getResources().getIdentifier("page_"+String.valueOf(position), "color", getPackageName());
			PagerTitleStrip pagerTitleStrip = (PagerTitleStrip) findViewById(R.id.pager_title_strip);
			
			Log.v("AUKPOS", "page_"+String.valueOf(position));
			pagerTitleStrip.setBackgroundResource(resId);
			
			if(position == 0) favSectionFragment.updateFavoriteEmptyView();
		}
	}
	
	public static class FavoriteSectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		
		ListView favoriteListView;
		View favoriteEmptyView;
		View favoriteView;
		
		FavoriteListAdapter favoriteListAdapter;
		Map<String, String> calcsToIds;
		Map<String, String> idsToCalcOptions;
		Map<String, String> idsToCategory;

		public FavoriteSectionFragment() {
		}
		
		public void updateFavoriteEmptyView() {
			if(favoriteView!= null && favoriteView.getVisibility() == View.VISIBLE) {
				TextView item = (TextView) favoriteView.findViewById(R.id.favorite_4);
				SlidingUpPanelLayout layout = (SlidingUpPanelLayout) favoriteView.findViewById(R.id.favorite_sliding_layout);            
	            layout.setPanelHeight(item.getHeight());
	            
	            //Log.v("AUK BOT", String.valueOf(item.getBottom()));
	            //Log.v("AUK PADBOT", String.valueOf(item.getPaddingBottom()));
	            //Log.v("AUK Height", String.valueOf(item.getHeight()));
	            
	            //item.setText(item.getText()+" "+String.valueOf(item.getBottom()) + " " + String.valueOf(item.getPaddingBottom()) + " " + String.valueOf(item.getHeight()));
			}
		}

		@Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
		}
		
		@Override
		public void onPause() {
			super.onPause();
						
			Context context = getActivity().getBaseContext();
			SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE).edit();
			
			int favListPosition = favoriteListView.getFirstVisiblePosition();
			editor.putInt(getString(R.string.fav_list_position), favListPosition);
			
			View itemView = favoriteListView.getChildAt(0);
			int favItemPosition = itemView == null ? 0 : itemView.getTop();
			editor.putInt(getString(R.string.fav_list_item), favItemPosition);
	        	
			editor.commit();
		}
		
		@Override
		public void onResume() {
			super.onResume();			
			
			Context context = getActivity().getBaseContext();
			SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE);
			
			int favListPosition = sharedPrefs.getInt(getString(R.string.fav_list_position), 0);
			int favItemPosition = sharedPrefs.getInt(getString(R.string.fav_list_item), 0);
			
			favoriteListView.setSelectionFromTop(favListPosition, favItemPosition);            
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.favorite_list,
					container, false);		
			
			Log.v("AUKASDLASDL", String.valueOf(rootView.getLayoutParams().height));
			
			favoriteListView = (ListView) rootView.findViewById(R.id.favorite_list);
			favoriteEmptyView = (LinearLayout) rootView.findViewById(R.id.favorite_empty);
			favoriteView = rootView;
			updateFavoriteList();			
			
			return rootView;
		}
		
		public void updateFavoriteList() {
			Context context = getActivity().getBaseContext();
			SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE);
			
			String[] catList = getResources().getStringArray(R.array.category_list);
			
			calcsToIds = new LinkedHashMap<String, String>();
			idsToCalcOptions = new LinkedHashMap<String, String>();
			idsToCategory = new LinkedHashMap<String, String>();
			List<String> calcList = new ArrayList<String>();
			
			Locale l = Locale.getDefault();
			
			for (String category : catList) {
				String categoryName = category.toLowerCase(l).replaceAll("[^a-z0-9_]","");
				String arrayName = categoryName+"_names";
	            int arrayId = getResources().getIdentifier(arrayName, "array", getActivity().getPackageName());
	            String [] calcs = getResources().getStringArray(arrayId);	                	
		        
		        for (String calcname : calcs) {
		        	String[] splitResult = calcname.split("\\|", 3);
		        	String calc = splitResult[2];
		        	String calcOptions = splitResult[1];
		        	String id = splitResult[0];
		        	
		        	if(sharedPrefs.getBoolean(id, false)) {
		        		calcList.add(calc);
			        	calcsToIds.put(calc, id);
			        	idsToCalcOptions.put(id, calcOptions);
			        	idsToCategory.put(id, categoryName);
		        	}		        	
		        }
			}
			
			Collections.sort(calcList);
			
			if(calcList.size() == 0) {
				/* Set the View different */
				/*calcList.add(context.getString(R.string.favorite_1));
				favoriteListAdapter = new FavoriteListAdapter(
		                getActivity(), calcList);				
				
				favoriteListView.setFooterDividersEnabled(false);*/
				
				favoriteEmptyView.setVisibility(View.VISIBLE);
				favoriteListView.setVisibility(View.GONE);
				
			}
			else {
				favoriteListAdapter = new FavoriteListAdapter(
		                getActivity(), calcList, calcsToIds, idsToCalcOptions, idsToCategory);
				
				favoriteListView.setFooterDividersEnabled(true);
				favoriteListView.setAdapter(favoriteListAdapter);	
				
				favoriteEmptyView.setVisibility(View.GONE);
				favoriteListView.setVisibility(View.VISIBLE);
			}
			
				
			
			/*favoriteListView.setOnItemClickListener(new OnItemClickListener() {
				 
	            public void onItemClick(AdapterView<?> parent, View v,
	                    int position, long id) {
	                final String selected = (String) favoriteListAdapter.getCalc(position);
	                Toast.makeText(getActivity().getBaseContext(), selected, Toast.LENGTH_LONG)
	                        .show();
	            }				
	        });*/
		}
	}
	
	
	public static class CategorySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		
	    List<String> categoryList;
	    List<String> calcList;
	    Map<String, List<String>> categoryCollection;
	    Map<String, String> calcsToIds;
	    Map<String, String> idsToCalcOptions;
	    Map<String, String> idsToCategory;
	    ExpandableListView categoryListView;
	    CategoryListAdapter categoryListAdapter;

		public CategorySectionFragment() {
		}
		
		@Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
		}
		
		@Override
		public void onPause() {
			super.onPause();
						
			Context context = getActivity().getBaseContext();
			SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE).edit();
			
			int categoryListPosition = categoryListView.getFirstVisiblePosition();
			editor.putInt(getString(R.string.category_expand_list_position), categoryListPosition);
			
			View itemView = categoryListView.getChildAt(0);
			int categoryItemPosition = itemView == null ? 0 : itemView.getTop();
			editor.putInt(getString(R.string.category_expand_list_item), categoryItemPosition);
			
			int groupCount = categoryListAdapter.getGroupCount();
//	        ArrayList<Integer> groupExpList = new ArrayList<Integer>();
			StringBuilder str = new StringBuilder();
			
			for(int i = 0; i < groupCount; ++i) {
	        	if(categoryListView.isGroupExpanded(i)) {
	        		//groupExpList.add(i);
	        		
	        		str.append(i).append(",");
	        	}
	        }
	        editor.putString(getString(R.string.category_expand_list), str.toString());
	        	
			editor.commit();
		}
		
		@Override
		public void onResume() {
			super.onResume();			
			
			Context context = getActivity().getBaseContext();
			SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE);
			StringTokenizer st = new StringTokenizer(sharedPrefs.getString(getString(R.string.category_expand_list), ""), ",");
			
			while(st.hasMoreTokens()) {
				categoryListView.expandGroup(Integer.parseInt(st.nextToken()));
			}
			
			int categoryListPosition = sharedPrefs.getInt(getString(R.string.category_expand_list_position), 0);
			int categoryItemPosition = sharedPrefs.getInt(getString(R.string.category_expand_list_item), 0);
			
			categoryListView.setSelectionFromTop(categoryListPosition, categoryItemPosition);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {			
			
			createGroupList();			 
	        createCollection();
	        
			View rootView = inflater.inflate(R.layout.category_list,
					container, false);			
			
			categoryListView = (ExpandableListView) rootView.findViewById(R.id.category_list);
			
			categoryListAdapter = new CategoryListAdapter(
	                getActivity(), categoryList, categoryCollection, calcsToIds, idsToCalcOptions, idsToCategory);			
			
			categoryListView.setAdapter(categoryListAdapter);			
			
			/*categoryListView.setOnChildClickListener(new OnChildClickListener() {
				 
	            public boolean onChildClick(ExpandableListView parent, View v,
	                    int groupPosition, int childPosition, long id) {
	                final String selected = (String) categoryListAdapter.getChild(
	                        groupPosition, childPosition);
	                Toast.makeText(getActivity().getBaseContext(), selected, Toast.LENGTH_LONG)
	                        .show();
	 
	                return true;
	            }
	        });*/
			
			
			return rootView;
		}
		
		public void updateCategoryListView() {
			if(categoryListView != null) {
				categoryListAdapter.notifyDataSetChanged();
			}
		}
		
		private void createGroupList() {
			String[] catList = getResources().getStringArray(R.array.category_list);
			categoryList = new ArrayList<String>();
			
			for (String category : catList) {			
				categoryList.add(category);
			}
	    }
	 
	    private void createCollection() {	        
	        
	        categoryCollection = new LinkedHashMap<String, List<String>>();
	        calcsToIds = new LinkedHashMap<String, String>();
	        idsToCalcOptions = new LinkedHashMap<String, String>();
	        idsToCategory = new LinkedHashMap<String, String>();
	        Locale l = Locale.getDefault();
	 
	        for (String category : categoryList) {
	        	String categoryName = category.toLowerCase(l).replaceAll("[^a-z0-9_]","");
	        	String arrayName = categoryName+"_names";
	            int arrayId = getResources().getIdentifier(arrayName, "array", getActivity().getPackageName());
	            String [] calcs = getResources().getStringArray(arrayId);
	            
	            loadChild(calcs, categoryName);	 
	            categoryCollection.put(category, calcList);
	        }
	    }
	 
	    private void loadChild(String[] category, String categoryName) {
	    	
	    	calcList = new ArrayList<String>();
	        for (String calcname : category) {
	        	String[] splitResult = calcname.split("\\|",3);
	        	String calc = splitResult[2];
	        	String calcOptions = splitResult[1];
	        	String id = splitResult[0];
	        	
	        	calcList.add(calc);
	        	calcsToIds.put(calc, id);
	        	idsToCalcOptions.put(id, calcOptions);
	        	idsToCategory.put(id, categoryName);
	        }
	        
	        Collections.sort(calcList);
	    }
	}

	public static class AllSectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";
		
	    List<String> calcList;
	    Map<String, String> calcsToIds;
	    Map<String, String> idsToCalcOptions;
	    Map<String, String> idsToCategory;
	    
	    ListView allListView;
	    AllListAdapter allListAdapter;

		public AllSectionFragment() {
		}
		
		@Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
		}
		
		@Override
		public void onPause() {
			super.onPause();
						
			Context context = getActivity().getBaseContext();
			SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE).edit();
			
			int allListPosition = allListView.getFirstVisiblePosition();
			editor.putInt(getString(R.string.all_list_position), allListPosition);
			
			View itemView = allListView.getChildAt(0);
			int allItemPosition = itemView == null ? 0 : itemView.getTop();
			editor.putInt(getString(R.string.all_list_item), allItemPosition);
	        	
			editor.commit();
		}
		
		@Override
		public void onResume() {
			super.onResume();			
			
			Context context = getActivity().getBaseContext();
			SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.finaniCalPrefs), Activity.MODE_PRIVATE);
			
			int allListPosition = sharedPrefs.getInt(getString(R.string.all_list_position), 0);
			int allItemPosition = sharedPrefs.getInt(getString(R.string.all_list_item), 0);
			
			allListView.setSelectionFromTop(allListPosition, allItemPosition);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
				     
			loadAllCalcs();
			
			View rootView = inflater.inflate(R.layout.all_list,
					container, false);			
			
			allListView = (ListView) rootView.findViewById(R.id.all_list);
			allListAdapter = new AllListAdapter(
	                getActivity(), calcList, calcsToIds, idsToCalcOptions, idsToCategory);
			
			allListView.setAdapter(allListAdapter);			
			
			/*allListView.setOnItemClickListener(new OnItemClickListener() {
				 
	            public void onItemClick(AdapterView<?> parent, View v,
	                    int position, long id) {
	                final String selected = (String) allListAdapter.getCalc(position);
	                Toast.makeText(getActivity().getBaseContext(), selected, Toast.LENGTH_LONG)
	                        .show();
	            }				
	        });*/
			
			return rootView;
		}
		
		public void updateAllCalcsListView() {
			allListAdapter.notifyDataSetChanged();
		}
		
		private void loadAllCalcs() {
			
			String[] catList = getResources().getStringArray(R.array.category_list);
			
			calcList = new ArrayList<String>();
			calcsToIds = new LinkedHashMap<String, String>();
			idsToCalcOptions = new LinkedHashMap<String, String>();
			idsToCategory = new LinkedHashMap<String, String>();
			
			Locale l = Locale.getDefault();
			
			for (String category : catList) {
				String categoryName = category.toLowerCase(l).replaceAll("[^a-z0-9_]","");
				String arrayName = categoryName+"_names";
	            int arrayId = getResources().getIdentifier(arrayName, "array", getActivity().getPackageName());
	            String [] calcs = getResources().getStringArray(arrayId);	                	
		        
		        for (String calcname : calcs) {
		        	String[] splitResult = calcname.split("\\|",3);
		        	String calc = splitResult[2];
		        	String calcOptions = splitResult[1];
		        	String id = splitResult[0];
		        	
		        	calcList.add(calc);
		        	calcsToIds.put(calc, id);
		        	idsToCalcOptions.put(id, calcOptions);
		        	idsToCategory.put(id, categoryName);
		        }
			}
			
			Collections.sort(calcList);
	    }
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_finanical_dummy,
					container, false);
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			dummyTextView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}
	}

}
