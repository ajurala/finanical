package com.aj.finanical;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with interest and duration in years
 */
public class Calc61 extends Calc {   

	EditText monthlyIncome = null;
	EditText duration_for_retirment = null;
	EditText yearly_increase = null;
    EditText inflation_before_retirment = null;
    EditText returns_expected_before_retirement = null;
    EditText duration_retirment = null;
    EditText inflation_during_retirment = null;
    EditText returns_expected_during_retirement = null;
    
    TextView monthlyIncome_name = null;
    TextView duration_for_retirment_name = null;
	TextView yearly_increase_name = null;
    TextView inflation_before_retirment_name = null;
    TextView returns_expected_before_retirement_name = null;
    TextView duration_retirment_name = null;
    TextView inflation_during_retirment_name = null;
    TextView returns_expected_during_retirement_name = null;
    
    TextView result_name = null;
    TextView result_extra_info = null;
    
    View yearly_increase_view = null;
    View returns_expected_before_retirement_view2 = null;;
    View results_view3 = null;   
    
    ArrayList<RelativeLayout> calc_result_fields = new ArrayList<RelativeLayout>();
    ArrayList<RelativeLayout> calc_result_values = new ArrayList<RelativeLayout>();
    ArrayList<ResultTextView> results = new ArrayList<ResultTextView>();

    String options;
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc61_monthly_income_value);    	
    	monthlyIncome_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc61_0_monthly_income_name);    	
    	monthlyIncome_name.setText(value);
    	
    	monthlyIncome = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration till retirement
    	v = findViewById(R.id.calc61_duration_for_retirment_value);    	
    	duration_for_retirment_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc61_0_duration_for_retirment_name);    	
    	duration_for_retirment_name.setText(value);
    	
    	duration_for_retirment = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Inflation before
    	v = findViewById(R.id.calc61_inflation_before_retirment_value);    	
    	inflation_before_retirment_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc61_0_inflation_before_retirment_name);    	
    	inflation_before_retirment_name.setText(value);
    	
    	inflation_before_retirment = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Yearly Increase
    	v = findViewById(R.id.calc61_yearly_increase_value);    	
    	yearly_increase_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc61_0_yearly_increase_name);    	
    	yearly_increase_name.setText(value);
    	
    	yearly_increase = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	yearly_increase_view = v;
    	
    	//Returns expected before
    	v = findViewById(R.id.calc61_returns_expected_before_retirement_value);    	
    	returns_expected_before_retirement_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_returns_expected_before_retirement_name);    	
    	returns_expected_before_retirement_name.setText(value);

    	returns_expected_before_retirement = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	returns_expected_before_retirement_view2 = v;
    	
    	//Duration in Years after
    	v = findViewById(R.id.calc61_duration_retirment_value);    	
    	duration_retirment_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_duration_retirment_name);    	
    	duration_retirment_name.setText(value);
    	
    	duration_retirment = (EditText) v.findViewById(R.id.calc_field_value);

    	//Inflation after
    	v = findViewById(R.id.calc61_inflation_during_retirment_value);    	
    	inflation_during_retirment_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc61_0_inflation_during_retirment_name);    	
    	inflation_during_retirment_name.setText(value);
    	
    	inflation_during_retirment = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Returns expected before
    	v = findViewById(R.id.calc61_returns_expected_during_retirement_value);    	
    	returns_expected_during_retirement_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_returns_expected_during_retirement_name);    	
    	returns_expected_during_retirement_name.setText(value);

    	returns_expected_during_retirement = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	
    	//Result List
    	//Result 1
    	v = findViewById(R.id.calc61_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_result_name);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 2
    	v = findViewById(R.id.calc61_result_value2);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_result_name2);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 3
    	v = findViewById(R.id.calc61_result_value3);
    	results_view3 = v;
    	
    	v = findViewById(R.id.calc61_result_value3_1);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_result_name3_1);    	
    	result_name.setText(value);
    	result_extra_info = result_name;

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	//Result 4
    	v = findViewById(R.id.calc61_result_value3_2);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc61_0_result_name3_2);    	
    	result_name.setText(value);

    	results.add((ResultTextView) v.findViewById(R.id.calc_field_value));
    	
    	calc_result_fields.add((RelativeLayout) v.findViewById(R.id.calc_field));
    	calc_result_values.add((RelativeLayout) v.findViewById(R.id.calc_value));
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};		

		monthlyIncome.addTextChangedListener(editTextWatcher);
		duration_for_retirment.addTextChangedListener(editTextWatcher);
		yearly_increase.addTextChangedListener(editTextWatcher);
	    inflation_before_retirment.addTextChangedListener(editTextWatcher);
	    returns_expected_before_retirement.addTextChangedListener(editTextWatcher);
	    duration_retirment.addTextChangedListener(editTextWatcher);
	    inflation_during_retirment.addTextChangedListener(editTextWatcher);
	    returns_expected_during_retirement.addTextChangedListener(editTextWatcher);	    
	   
    	options = calc_options.substring(calc_options.indexOf('.') + 1);
    	
    	Log.v("AUKOPT", options);
    	
    	if(options.equals("0")) {    		
    		yearly_increase_view.setVisibility(View.GONE);
    		returns_expected_before_retirement_view2.setVisibility(View.GONE);
    		
    		results_view3.setVisibility(View.GONE);
    	}
    	
    	setResultViews(calc_result_fields, calc_result_values, results);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();		
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double monthly_income, target_years_pre, inflation_pre, return_pre, income_rise, target_years_pro, inflation_pro, return_pro;
		double monthly_income_required_ret, yearly_income_required_ret, future_value_incomes_at_ret, monthly_investment, constant_monthly_investment;
		
		try{
			monthly_income = Double.parseDouble(monthlyIncome.getText().toString());
			target_years_pre = Double.parseDouble(duration_for_retirment.getText().toString());
			inflation_pre = Double.parseDouble(inflation_before_retirment.getText().toString()) / 100;
			return_pre = Double.parseDouble(returns_expected_before_retirement.getText().toString()) / 100;
			
			income_rise = Double.parseDouble(yearly_increase.getText().toString()) / 100;
			
			target_years_pro = Double.parseDouble(duration_retirment.getText().toString());
			inflation_pro = Double.parseDouble(inflation_during_retirment.getText().toString()) / 100;
			return_pro = Double.parseDouble(returns_expected_during_retirement.getText().toString()) / 100;
			
			if(monthly_income == 0
				|| target_years_pre == 0
				|| inflation_pre == 0
				|| return_pro == 0
				|| target_years_pro == 0
				|| inflation_pro == 0) {
				monthly_income_required_ret = yearly_income_required_ret = future_value_incomes_at_ret = monthly_investment = constant_monthly_investment = 0;
			}
			else {
				
				if (return_pre==income_rise)
				{
					return_pre=income_rise + 0.00001;
				}

				if (return_pro==inflation_pro)
				{
					return_pro=inflation_pro + 0.00001;
				}
				
				yearly_income_required_ret= ((monthly_income * 12) * Math.pow((1 + inflation_pre),target_years_pre));
				monthly_income_required_ret = yearly_income_required_ret / 12;
				
				double ret_factor = ((Math.pow((1+return_pro),target_years_pro) - Math.pow((1+inflation_pro) , target_years_pro))/(return_pro-inflation_pro))/(Math.pow((1+return_pro),target_years_pro));
				future_value_incomes_at_ret = (yearly_income_required_ret * ret_factor) ;				
				
				if(results_view3.getVisibility() == View.VISIBLE) {
					double investment_ret_factor = (Math.pow((1+return_pre),target_years_pre) - Math.pow((1+income_rise) , target_years_pre))/(return_pre-income_rise);
					monthly_investment = (future_value_incomes_at_ret /investment_ret_factor) / 12.0;
					constant_monthly_investment = (future_value_incomes_at_ret*return_pre/(Math.pow((1+return_pre),target_years_pre)-1)) / 12.0;
				}
				else {
					monthly_investment = constant_monthly_investment = 0;
				}

			}
			
			
		}catch(NumberFormatException e) {
			monthly_income_required_ret = yearly_income_required_ret = future_value_incomes_at_ret = monthly_investment = constant_monthly_investment = 0;
		}
		
		NumberFormat f = NumberFormat.getInstance(Locale.US);		
		
		results.get(0).setText(f.format(future_value_incomes_at_ret));
		results.get(1).setText(f.format(monthly_income_required_ret));
		
		if(results_view3.getVisibility() == View.VISIBLE) {
			results.get(2).setText(f.format(monthly_investment));		
			results.get(3).setText(f.format(constant_monthly_investment));
		}
		
		//TODO - Improve this text setting which happens always ...
		result_extra_info.setText(yearly_increase.getText().toString() + " " + getString(R.string.calc61_0_result_name3_1));
		
		
	}	
}