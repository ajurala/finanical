package com.aj.finanical;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with initialValue and duration in years
 */
public class Calc81 extends Calc {   

	EditText currentValue = null;
    EditText initialValue = null;
    EditText duration = null;
    
    ResultTextView result = null;
    
    TextView currentValue_name = null;
    TextView initialValue_name = null;
    TextView duration_name = null;
    
    TextView result_name = null;
    
    RelativeLayout calc_result_field = null;
    RelativeLayout calc_result_value = null;    
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc81_current_value);    	
    	currentValue_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value =getString(R.string.calc81_0_current_value_name);    	
    	currentValue_name.setText(value);
    	
    	currentValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//InitialValue 
    	v = findViewById(R.id.calc81_inital_value);    	
    	initialValue_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc81_0_initial_value_name);    	
    	initialValue_name.setText(value);

    	initialValue = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Duration
    	v = findViewById(R.id.calc81_duration_value);    	
    	duration_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc81_0_duration_name);    	
    	duration_name.setText(value);
    	
    	duration = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//Result
    	v = findViewById(R.id.calc81_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc81_0_result_name);    	
    	result_name.setText(value);

    	result = (ResultTextView) v.findViewById(R.id.calc_field_value);
    	
    	calc_result_field = (RelativeLayout) v.findViewById(R.id.calc_field);
    	calc_result_value = (RelativeLayout) v.findViewById(R.id.calc_value);
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
    	currentValue.addTextChangedListener(editTextWatcher);
    	initialValue.addTextChangedListener(editTextWatcher);
    	duration.addTextChangedListener(editTextWatcher);
    	
    	setResultView(calc_result_field, calc_result_value, result);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double iv, cv, ret;
		int years;
		
		try{
			cv = Double.parseDouble(currentValue.getText().toString());
			iv = Double.parseDouble(initialValue.getText().toString());
			years = Integer.parseInt(duration.getText().toString());
			
			ret = (Math.pow(cv/iv, 1.0/years) - 1 )*100;
		}catch(NumberFormatException e) {
			ret = 0;
		}
		
		setResultText(ret);
	}
}
