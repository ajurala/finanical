package com.aj.finanical.adapters;

import java.util.List;
import java.util.Map;
 
import com.aj.finanical.FinaniCal;
import com.aj.finanical.R;
 
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ToggleButton;
import android.widget.TextView;


public class AmortizationListAdapter extends BaseExpandableListAdapter {
	private Activity context;
    private Map<String, List<Amortize>> amortizationCollections;
    private List<Amortize> amortization;
 
    public AmortizationListAdapter(Activity context, List<Amortize> amortization,
            Map<String, List<Amortize>> amortizationCollections) {
        this.context = context;
        this.amortizationCollections = amortizationCollections;
        this.amortization = amortization;        
    }
 
    public Object getChild(int groupPosition, int childPosition) {
        return amortizationCollections.get(amortization.get(groupPosition).getMonth()).get(childPosition);
    }
 
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
    	Log.v("AUK CV GP", String.valueOf(groupPosition));
    	Log.v("AUK CV CP", String.valueOf(childPosition));
        final Amortize am = (Amortize) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();
 
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.amortization, parent, false);
        }
 
        TextView item = (TextView) convertView.findViewById(R.id.month);
        item.setText(am.getMonth());
        
        item = (TextView) convertView.findViewById(R.id.interest);
        item.setText(am.getInterest());
        
        item = (TextView) convertView.findViewById(R.id.principal);
        item.setText(am.getPrincipal());
        
        item = (TextView) convertView.findViewById(R.id.balance);
        item.setText(am.getBalance());
        
        return convertView;
    }
 
    public int getChildrenCount(int groupPosition) {
    	//Log.v("AUK GM", amortization.get(groupPosition).getMonth());
    	//Log.v("AUM CM get", String.valueOf(amortizationCollections.get(amortization.get(groupPosition).getMonth()).size()));
    	List<Amortize> am = amortizationCollections.get(amortization.get(groupPosition).getMonth());
    	if(am != null)
    		return amortizationCollections.get(amortization.get(groupPosition).getMonth()).size();
    	else
    		return 0;
    }
 
    public Object getGroup(int groupPosition) {
        return amortization.get(groupPosition);
    }
 
    public int getGroupCount() {
    	Log.v("AUK GC", String.valueOf(amortization.size()));
        return amortization.size();
    }
 
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
    	Log.v("AUK GV GP", String.valueOf(groupPosition));
    	Amortize am = (Amortize) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.amortization,
            		 parent, false);
        }   
        
        TextView item = (TextView) convertView.findViewById(R.id.month);
        item.setText(am.getMonth());
        
        item = (TextView) convertView.findViewById(R.id.interest);
        item.setText(am.getInterest());
        
        item = (TextView) convertView.findViewById(R.id.principal);
        item.setText(am.getPrincipal());
        
        item = (TextView) convertView.findViewById(R.id.balance);
        item.setText(am.getBalance());
        
        return convertView;
    }
 
    public boolean hasStableIds() {
        return true;
    }
 
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }    
    
}