package com.aj.finanical.adapters;

import java.util.List;
import java.util.Map;

import com.aj.finanical.FinaniCal;
import com.aj.finanical.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

public class AllListAdapter extends ArrayAdapter<String> {
	
	private Activity context;
	private List<String> calcs;
	private Map<String, String> calcsToIds;
	private Map<String, String> idsToCalcOptions;
	private Map<String, String> idsToCategory;
	
	public AllListAdapter(Activity context,
	        List<String> calcs, Map<String, String> calcsToIds, Map<String, String> idsToCalcOptions, Map<String, String> idsToCategory) {
	      super(context, R.layout.calc, calcs);
	      
	      this.context = context;
	      this.calcs = calcs;
	      this.calcsToIds = calcsToIds;
	      this.idsToCalcOptions = idsToCalcOptions;
	      this.idsToCategory = idsToCategory;
	}
	
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		final String calc = calcs.get(position);
		
        LayoutInflater inflater = context.getLayoutInflater();
 
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.calc, null);
        }
 
        TextView item = (TextView) convertView.findViewById(R.id.calc);
 
        
        final ToggleButton fav = (ToggleButton) convertView.findViewById(R.id.toggle);
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE);
        
        fav.setChecked(sharedPrefs.getBoolean(calcsToIds.get(calc), false));
        
        fav.setOnClickListener(new OnClickListener() {
 
        	@Override
            public void onClick(View v) {
        		
        		SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE).edit();
        		if(fav.isChecked()) {
                    editor.putBoolean(calcsToIds.get(calc), true); // value to store
        		}
        		else {
        			editor.remove(calcsToIds.get(calc));
        		}
        		
        		editor.commit();
        		
        		//if(context instanceof FinaniCal) {
        			FinaniCal finaniCal = (FinaniCal) context;        		
        			finaniCal.updateFavoriteList();
        			finaniCal.updateCategoryList();
        		//}
        		
            }
        });
 
        item.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FinaniCal finaniCal = (FinaniCal) context;
				finaniCal.showCalc(idsToCalcOptions.get(calcsToIds.get(calc)), calc, idsToCategory.get(calcsToIds.get(calc)));
			}
		});

        item.setText(calc);
        return convertView;
	  }
	
	public String getCalc(int position) {
		return calcs.get(position);
	}
}
