package com.aj.finanical.adapters;

import java.util.List;
import java.util.Map;
 
import com.aj.finanical.FinaniCal;
import com.aj.finanical.R;
 
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ToggleButton;
import android.widget.TextView;


public class CategoryListAdapter extends BaseExpandableListAdapter {
	private Activity context;
    private Map<String, List<String>> categoryCollections;
    private List<String> category;
    private Map<String, String> calcsToIds;
    private Map<String, String> idsToCalcOptions;
    private Map<String, String> idsToCategory;
 
    public CategoryListAdapter(Activity context, List<String> category,
            Map<String, List<String>> categoryCollections, Map<String, String> calcsToIds, Map<String, String> idsToCalcOptions, Map<String, String> idsToCategory) {
        this.context = context;
        this.categoryCollections = categoryCollections;
        this.category = category;
        this.calcsToIds = calcsToIds;
        this.idsToCalcOptions = idsToCalcOptions;
        this.idsToCategory = idsToCategory;
    }
 
    public Object getChild(int groupPosition, int childPosition) {
        return categoryCollections.get(category.get(groupPosition)).get(childPosition);
    }
 
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
        final String calc = (String) getChild(groupPosition, childPosition);
        LayoutInflater inflater = context.getLayoutInflater();
 
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.calc, null);
        }
 
        /*String color = idsToCategory.get(calcsToIds.get(calc)) + "_color";
        int resId = context.getResources().getIdentifier(color, "color", context.getPackageName());
        convertView.setBackgroundResource(resId);*/
        TextView item = (TextView) convertView.findViewById(R.id.calc);        
        
        final ToggleButton fav = (ToggleButton) convertView.findViewById(R.id.toggle);
        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE);
        
        fav.setChecked(sharedPrefs.getBoolean(calcsToIds.get(calc), false));
        
        fav.setOnClickListener(new OnClickListener() {
 
        	@Override
            public void onClick(View v) {
        		
        		SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE).edit();
        		if(fav.isChecked()) {
                    editor.putBoolean(calcsToIds.get(calc), true); // value to store
        		}
        		else {
        			editor.remove(calcsToIds.get(calc));
        		}
        		
        		editor.commit();
        		
        		//if(context instanceof FinaniCal) {
        			FinaniCal finaniCal = (FinaniCal) context;        		
        			finaniCal.updateFavoriteList();
        			finaniCal.updateAllCalcsList();
        		//}
        		
            }
        });
        
        item.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FinaniCal finaniCal = (FinaniCal) context;
				finaniCal.showCalc(idsToCalcOptions.get(calcsToIds.get(calc)), calc, idsToCategory.get(calcsToIds.get(calc)));
			}
		});
 
        item.setText(calc);
        return convertView;
    }
 
    public int getChildrenCount(int groupPosition) {
        return categoryCollections.get(category.get(groupPosition)).size();
    }
 
    public Object getGroup(int groupPosition) {
        return category.get(groupPosition);
    }
 
    public int getGroupCount() {
        return category.size();
    }
 
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String categoryName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.category,
                    null);
        }
        
        /*final String calc = (String) getChild(groupPosition, 0);
        String color = idsToCategory.get(calcsToIds.get(calc)) + "_color";
        int resId = context.getResources().getIdentifier(color, "color", context.getPackageName());*/
        convertView.setBackgroundResource(R.color.category_color);
        
        TextView item = (TextView) convertView.findViewById(R.id.category);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(categoryName);
        return convertView;
    }
 
    public boolean hasStableIds() {
        return true;
    }
 
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
