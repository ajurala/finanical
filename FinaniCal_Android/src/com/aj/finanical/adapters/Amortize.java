package com.aj.finanical.adapters;

public class Amortize{
	String month, interest, principal, balance;
	
	public Amortize(String month, String interest, String principal, String balance) {
		this.month = month;
		this.interest = interest;
		this.principal = principal;
		this.balance = balance;
	}
	
	public String getMonth() {
		return month;
	}
	
	public String getInterest() {
		return interest;
	}
	
	public String getPrincipal() {
		return principal;
	}
	
	public String getBalance() {
		return balance;
	}
}
