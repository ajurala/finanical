package com.aj.finanical.adapters;

import java.util.List;
import java.util.Map;

import com.aj.finanical.FinaniCal;
import com.aj.finanical.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class FavoriteListAdapter extends ArrayAdapter<String> {
	
	private Activity context;
	private List<String> calcs;
	private Map<String, String> calcsToIds;
	private Map<String, String> idsToCalcOptions;
	private Map<String, String> idsToCategory;
	
	boolean favListEmpty = false;
	
	public FavoriteListAdapter(Activity context,
	        List<String> calcs, Map<String, String> calcsToIds, Map<String, String> idsToCalcOptions, Map<String, String> idsToCategory) {
	      super(context, R.layout.calc, calcs);
	      
	      this.context = context;
	      this.calcs = calcs;
	      this.calcsToIds = calcsToIds;
	      this.idsToCalcOptions = idsToCalcOptions;
	      this.idsToCategory = idsToCategory;
	      
	      favListEmpty = false;
	}
	
	public FavoriteListAdapter(Activity context,
	        List<String> calcs) {
	      super(context, R.layout.favorite, calcs);
	      
	      this.context = context;
	      this.calcs = calcs;
	      
	      favListEmpty = true;
	}
	
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		final String calc = calcs.get(position);		
		
		
        LayoutInflater inflater = context.getLayoutInflater();
 
        if (convertView == null) {
        	if(favListEmpty) {
        		convertView = inflater.inflate(R.layout.favorite, parent, false);
        		Log.v("AUKPARAMSss", String.valueOf(convertView.getLayoutParams().height));
        		Log.v("AUKPARAMPppp", String.valueOf(parent.getLayoutParams().height));
        	}
        	else
        		convertView = inflater.inflate(R.layout.calc, parent, false);
        }
 
        if(!favListEmpty) {
        
        	TextView item = (TextView) convertView.findViewById(R.id.calc);
	        
	        final ToggleButton fav = (ToggleButton) convertView.findViewById(R.id.toggle);        
	        
	        fav.setChecked(true);
	        
	        fav.setOnClickListener(new OnClickListener() {
	 
	        	@Override
	            public void onClick(View v) {
	        		
	        		final SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.favPrefs), Activity.MODE_PRIVATE).edit();
	        		if(!fav.isChecked()) {
	        			
	        			AlertDialog.Builder builder = new Builder(context);
	        			builder.setMessage("Are you sure to remove this calculator from Favorite List?");
	        			builder.setTitle("Confirmation Dialog");
	
	        			builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
	        				public void onClick(DialogInterface dialog, int which) {
	                        
	        					editor.remove(calcsToIds.get(calc));
	        					editor.commit();
	        	        		
	        	        		//if(context instanceof FinaniCal) {
	        	        			FinaniCal finaniCal = (FinaniCal) context;
	        	        			
	        	        			finaniCal.updateFavoriteList();
	        	        			finaniCal.updateAllCalcsList();
	        	        			finaniCal.updateCategoryList();
	        	        		//}
	        	        	
	        				}
	        			});	
	
	        			builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
	        				public void onClick(DialogInterface dialog, int which) {
	        					dialog.cancel();
	        					fav.setChecked(true);
	        				}
	        			});	
	
	        			builder.create().show();
	        			
	        		}
	        		
	        			
	            }
	        });
	        
	        item.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					FinaniCal finaniCal = (FinaniCal) context;
					finaniCal.showCalc(idsToCalcOptions.get(calcsToIds.get(calc)), calc, idsToCategory.get(calcsToIds.get(calc)));
				}
			});
	        
	        item.setText(calc);
        }
        
        
        return convertView;
	  }
	
	
	@Override
	public boolean isEnabled(int position) {
	    return !favListEmpty;
	}
	
	public String getCalc(int position) {
		return calcs.get(position);
	}
}
