package com.aj.finanical;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.aj.finanical.ResultTextView.OnTextViewSizeChangedListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Calc extends Activity {
	
    ArrayList<ResultTextView> resultTextViews = null;
    
    protected String calcId, calc_options, calc_preference;
    protected int calcType = 0;
    
    ImageView dragView = null;
    
    SlidingUpPanelLayout slidingLayout = null;
    boolean calculationNeeded = false;
    
    Calc() {
    	calcId = getClass().getSimpleName();
    	calc_preference = calcId;
    	
    	Log.v("AUKKKK", calcId);
    }
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.calc_base);        
        
        //Inflate the calc
        ViewGroup mainLayout = (ViewGroup) findViewById(R.id.calc_scroll);
        LayoutInflater inflater = 
                      (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        Locale l = Locale.getDefault();
        int resId = getResources().getIdentifier(calcId.toLowerCase(l), "layout", getPackageName());
        inflater.inflate(resId, mainLayout, true);
        
        //Inflate the calc_result
        mainLayout = (ViewGroup) findViewById(R.id.result);
        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        resId = getResources().getIdentifier(calcId.toLowerCase(l)+"_result", "layout", getPackageName());
        inflater.inflate(resId, mainLayout, true);
        
        //Get the dragView
        dragView = (ImageView)findViewById(R.id.dragView); //Layout to slide
        
        //Set the calc name
        TextView item = (TextView) findViewById(R.id.calc_name);
        item.setText(getIntent().getExtras().getString(getString(R.string.name_of_calc)));
        
        //Set the color of the calculator
        String category = getIntent().getExtras().getString(getString(R.string.category_of_calc));
        
        resId = getResources().getIdentifier(category+"_color", "color", getPackageName());
        item.setBackgroundResource(resId);
        
        
        //Remove scrollview if type 1 calc
        if(calcType == 1) {
        	ViewGroup scrollChildLayout = (ViewGroup) mainLayout;
        	((ViewGroup)scrollChildLayout.getParent()).removeView(mainLayout);
        	
        	// Get scroll view out of the way
        	View scrollView = findViewById(R.id.scrollview);
        	scrollView.setVisibility(View.GONE);

        	// Put the child view into scrollview's parent view
        	ViewGroup parentLayout = (ViewGroup)findViewById(R.id.sliding_layout);
        			
        	parentLayout.addView(scrollChildLayout);
        }
        
        //Save the options and preferences for future use
        calc_options = getIntent().getExtras().getString(getString(R.string.options_to_calc));
        
        String prefsOptions = calc_options.substring(0, calc_options.indexOf('.'));
        calc_preference = calcId+"." + prefsOptions;
        
        Log.v("AUKOOOO", calc_options);
        Log.v("AUKASDK", calc_preference);
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) {
        	// Set the collapsed height 
            slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);            
            slidingLayout.setPanelHeight(dragView.getHeight());
            
            //Set the listener
            SlideUpDownPanelListener slideUpDownPanelListener = new SlideUpDownPanelListener();
            slidingLayout.setPanelSlideListener(slideUpDownPanelListener);
            
            //Set the dragview to intended sliding view instead of the whole view
            //layout.setDragView(dragView); 
            
            slidingLayout.expandPane();
        }
			
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		saveEditTexts();		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		updateEditTexts();
	}	
	
	@Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        //super.onSaveInstanceState(savedInstanceState); 
        
		saveEditTexts();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	//super.onRestoreInstanceState(savedInstanceState);
    	
    	updateEditTexts();
    }
	
    private void saveEditTexts()
    {
    	//Get all the editTexts and save their values
    	View v = findViewById(android.R.id.content);
    	saveEditTexts((ViewGroup)v, "Calc");
    }
	private void saveEditTexts(ViewGroup v, String key)
	{
		
				
		if(v.getId() != View.NO_ID){
			String IdAsString = v.getResources().getResourceEntryName(v.getId());
			key += "."+IdAsString;
		}	
		
	    for (int i = 0; i < v.getChildCount(); i++)
	    {
	        Object child = v.getChildAt(i); 
	        if (child instanceof EditText)
	        {	        	
	            EditText e = (EditText)child;	           
	            String value = e.getText().toString();
	            
	            if(e.getId() != View.NO_ID){
	    			String IdAsString = e.getResources().getResourceEntryName(e.getId());
	    			key += "."+IdAsString;
	    		}
	            
	            SharedPreferences.Editor editor = getSharedPreferences(calc_preference, Activity.MODE_PRIVATE).edit();
	            editor.putString(key, value);
	            editor.commit();
	            
	            Log.v("AUKSave", key);
	        }
	        else if(child instanceof ViewGroup)
	        {	        	
	            saveEditTexts((ViewGroup)child, key);  // Recursive call.	            
	        }
	    }
	}
	
	private void updateEditTexts()
	{
		//Get all the editTexts values from preference and put their values
		View v = findViewById(android.R.id.content);
		updateEditTexts((ViewGroup)v, "Calc");
		
		//SharedPreferences.Editor editor = getSharedPreferences(calc_preference, Activity.MODE_PRIVATE).edit();
        //editor.clear();
        //editor.commit();
	}
	private void updateEditTexts(ViewGroup v, String key)
	{
		if(v.getId() != View.NO_ID){
			String IdAsString = v.getResources().getResourceEntryName(v.getId());
			key += "."+IdAsString;
		}
		
	    for (int i = 0; i < v.getChildCount(); i++)
	    {
	        Object child = v.getChildAt(i); 
	        if (child instanceof EditText)
	        {	        	
	            EditText e = (EditText)child;	     
	            
	            if(e.getId() != View.NO_ID){
	    			String IdAsString = e.getResources().getResourceEntryName(e.getId());
	    			key += "."+IdAsString;
	    		}
	            
	            SharedPreferences prefs = getSharedPreferences(calc_preference, Activity.MODE_PRIVATE);
	            e.setText(prefs.getString(key, "0"));
	            
	            Log.v("AUKUpdate", key);
	        }
	        else if(child instanceof ViewGroup)
	        {	        	
	        	updateEditTexts((ViewGroup)child, key);  // Recursive call.	            
	        }
	    }
	}
	
	protected void setResultViews(final ArrayList<RelativeLayout> calc_result_fields, final ArrayList<RelativeLayout> calc_result_values, ArrayList<ResultTextView> results) {
		resultTextViews = results;
		
		int i = 0;
		for (ResultTextView resultTextView : resultTextViews) {
			final RelativeLayout calc_result_field = calc_result_fields.get(i);
			final RelativeLayout calc_result_value = calc_result_values.get(i);
			
			resultTextView.setCustomEventListener(new OnTextViewSizeChangedListener() {
				
				public void onTextViewSizeChangedEvent() {
					Log.v("AUK RESULT", "called for change in result");
					modifyLayout(calc_result_field, calc_result_value);
					
				}
			});
			
			i++;
		}
	}
	
	protected void setResultView(final RelativeLayout calc_result_field, final RelativeLayout calc_result_value, ResultTextView result) {
		resultTextViews = new ArrayList<ResultTextView>();
		resultTextViews.add(result);		
			
		result.setCustomEventListener(new OnTextViewSizeChangedListener() {
				
				public void onTextViewSizeChangedEvent() {
					modifyLayout(calc_result_field, calc_result_value);
					
				}
			});
			
			
	}
	
	protected void modifyLayout(RelativeLayout calc_result_field, RelativeLayout calc_result_value) {
		if(calc_result_field != null && calc_result_value != null) {			
			int x1 = calc_result_field.getRight();
			int x2 = calc_result_value.getLeft();
			
			
	
			Log.v("AUK", "Modifying layout??");
			
			Log.v("AUK", String.valueOf(x1));
			Log.v("AUK", String.valueOf(x2));
			
			RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) calc_result_value.getLayoutParams();
			
			if(x1 >= x2) {
				Log.v("AUK", "Setting below");
				p.addRule(RelativeLayout.BELOW, R.id.calc_field);			
			}
			else {
				Log.v("AUK", "UnSetting below");
				p.addRule(RelativeLayout.BELOW, 0);
			}
			
			calc_result_value.setLayoutParams(p);
			//calc_result_value.postInvalidate();
		}
	}
	
	protected void setResultTexts(ArrayList<Object> results) {
		NumberFormat f = NumberFormat.getInstance(Locale.US);
		
		int i = 0;
		for (ResultTextView resultTextView : resultTextViews) {
			resultTextView.setText(f.format(results.get(i)));
			i++;
		}
	}
	
	protected void setResultText(Object result) {
		NumberFormat f = NumberFormat.getInstance(Locale.US);		
		
		resultTextViews.get(0).setText(f.format(result));
	}
	
	protected void calculateResult() {
		
	}
	
	@Override
	public void onBackPressed() {
	    // your code.
		if(slidingLayout != null && slidingLayout.isExpanded()) {
			slidingLayout.collapsePane();
		}
		else {
			super.onBackPressed();
		}
	}
	
	protected class SlideUpDownPanelListener extends SlidingUpPanelLayout.SimplePanelSlideListener {
		@Override
		public void onPanelSlide(View panel, float slideOffset) {
			if(calculationNeeded)
				calculateResult();
		}
		@Override
        public void onPanelCollapsed(View panel) {
			dragView.setImageResource(R.drawable.ic_action_expand_pure_white);
			
        }
        @Override
        public void onPanelExpanded(View panel) {
        	dragView.setImageResource(R.drawable.ic_action_collapse_pure_white);
        }
	}
	

	
	/* Useful later
	String IdAsString = v.getResources().getResourceName(v.getId());
	Log.v("AUKSSSS", IdAsString);
	int arrayId = getResources().getIdentifier(IdAsString, "id", getPackageName());
	Log.v("AUKSSSS1", String.valueOf(arrayId));*/
}
