package com.aj.finanical;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/*
 * This calculator is for getting the current value for the future value provided by the user along with inflation and duration in years
 */
public class Calc82 extends Calc {   

	EditText returns = null;
    EditText inflation = null;
    
    ResultTextView result = null;
    
    TextView returns_name = null;
    TextView inflation_name = null;
    
    TextView result_name = null;
    
    RelativeLayout calc_result_field = null;
    RelativeLayout calc_result_value = null;    
    
	@SuppressLint("CutPasteId")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
    	View v;
    	String value;
    	//Amount
    	v = findViewById(R.id.calc82_return_value);    	
    	returns_name = (TextView)v.findViewById(R.id.calc_field_name);    	
    	
    	value = getString(R.string.calc82_0_return_name);    	
    	returns_name.setText(value);
    	
    	returns = (EditText) v.findViewById(R.id.calc_field_value);
    	
    	//inflation
    	v = findViewById(R.id.calc82_inflation_value);    	
    	inflation_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc82_0_inflation_name);    	
    	inflation_name.setText(value);

    	inflation = (EditText) v.findViewById(R.id.calc_field_value);    	
    	
    	
    	//Result
    	v = findViewById(R.id.calc82_result_value);
    	result_name = (TextView)v.findViewById(R.id.calc_field_name);
    	
    	value = getString(R.string.calc82_0_result_name);    	
    	result_name.setText(value);

    	result = (ResultTextView) v.findViewById(R.id.calc_field_value);
    	
    	calc_result_field = (RelativeLayout) v.findViewById(R.id.calc_field);
    	calc_result_value = (RelativeLayout) v.findViewById(R.id.calc_value);
    	
    	TextWatcher editTextWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//calculateResult();
				calculationNeeded = true;
			}
		};
		
    	returns.addTextChangedListener(editTextWatcher);
    	inflation.addTextChangedListener(editTextWatcher);
    	
    	setResultView(calc_result_field, calc_result_value, result);
    }
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}	
	
	/*@Override
	public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(hasFocus) 			
			calculateResult();
	}*/
	
	protected void calculateResult() {
		double rets, infltin, res;
		
		try{
			rets = Double.parseDouble(returns.getText().toString());
			infltin = Double.parseDouble(inflation.getText().toString());
			
			res = (((1.0 + (rets/100.0)) / (1.0 + (infltin/100.0))) - 1) * 100;
		}catch(NumberFormatException e) {
			res = 0;
		}
		
		setResultText(res);
	}
}
